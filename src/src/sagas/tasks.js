import {takeEvery, call, put, select} from 'redux-saga/effects'; // takeLatest, all, fork, spawn, take
import {
	GET_TASKS_REQUEST,
	GET_TASKS_SUCCESS,
	ADD_TASK_REQUEST,
	ADD_TASK_SUCCESS,
	DELETE_TASK_REQUEST,
	DELETE_TASK_SUCCESS,
	UPDATE_TASK_REQUEST,
	UPDATE_TASK_SUCCESS,
	CLOSE_ALL_TASKS_REQUEST,
	CLOSE_ALL_TASKS_SUCCESS,
	OPEN_ALL_TASKS_REQUEST,
	OPEN_ALL_TASKS_SUCCESS,
	TASK_TRANSACTION_FAILED
} from '../actions/tasks';
import {ADD_LOG_REQUEST} from '../actions/logs';

/** Calculate and return next ID */
export function calculateNextId (tasks) {
	let max = tasks.length > 0 ? Math.max.apply(Math, tasks.map(x => x.id)) : 0;
	return max + 1;
}

/** Return data from state */
export const fetchState = (state) => state.tasks;

/** List all existing tasks */
export function* getTasks () {
	try {
		const tasks = yield select(fetchState);
		yield put({ type: GET_TASKS_SUCCESS, tasks });
	} catch (error) {
		yield put({ type: TASK_TRANSACTION_FAILED, error:true, payload: error });
	}
}

/** Add new task */
export function* createTask (action) {
	try {
		const tasks = yield select(fetchState);
		const id = yield call(calculateNextId, tasks);
		const {item} = action;
		yield put({ type: ADD_TASK_SUCCESS, payload: { task: { id, item, finished: false}}});
		yield put({ type: ADD_LOG_REQUEST, payload: { operation: 'NEW', task: { id, item, finished: false}}})
	} catch (error) {
		yield put({ type: TASK_TRANSACTION_FAILED, error:true, payload: error });
	}
}

/** Remove task by ID */
export function* deleteTask (action) {
	try {
		const tasks = yield select(fetchState);
		const {id} = action;
		const removed = tasks.filter(x => x.id === id)[0];
		yield put({ type: DELETE_TASK_SUCCESS, payload: { task: { id }}});
		yield put({ type: ADD_LOG_REQUEST, payload: { operation: 'REMOVED', task: removed }})
	} catch (error) {
		yield put({ type: TASK_TRANSACTION_FAILED, error:true, payload: error });
	}
}

/** Update task finished status by ID */
export function* updateTask (action) {
	try {
		const {task} = action;
		const finished = !task.finished
		const operation = finished ? 'DONE' : 'UNDONE'; // Before status change
		yield put({ type: UPDATE_TASK_SUCCESS, payload: { task: { id: task.id, finished }}});
		yield put({ type: ADD_LOG_REQUEST, payload: { operation, task }})
	} catch (error) {
		yield put({ type: TASK_TRANSACTION_FAILED, error:true, payload: error });
	}
}

/** Close all tasks setting finished to 'true' */
export function* closeAllTasks () {
	try {
		yield put({ type: CLOSE_ALL_TASKS_SUCCESS });
		yield put({ type: ADD_LOG_REQUEST, payload: { operation: "ALL DONE", task: { id: 'ALL', item: 'ALL TASKS' }}});
	} catch (error) {
		yield put({ type: TASK_TRANSACTION_FAILED, error:true, payload: error });
	}
}

/** Open all tasks setting finished to 'false' */
export function* openAllTasks () {
	try {
		yield put({ type: OPEN_ALL_TASKS_SUCCESS });
		yield put({ type: ADD_LOG_REQUEST, payload: { operation: "ALL UNDONE", task: { id: 'ALL', item: 'ALL TASKS' }}});
	} catch (error) {
		yield put({ type: TASK_TRANSACTION_FAILED, error:true, payload: error });
	}
}

export default function* rootSaga() {
	yield takeEvery(ADD_TASK_REQUEST, createTask);
	yield takeEvery(GET_TASKS_REQUEST, getTasks);
	yield takeEvery(DELETE_TASK_REQUEST, deleteTask);
	yield takeEvery(UPDATE_TASK_REQUEST, updateTask);
	yield takeEvery(CLOSE_ALL_TASKS_REQUEST, closeAllTasks);
	yield takeEvery(OPEN_ALL_TASKS_REQUEST, openAllTasks);
}
