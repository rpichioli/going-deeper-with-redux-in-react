import {takeEvery, call, put, select} from 'redux-saga/effects';
import {
	GET_LOGS_REQUEST,
	GET_LOGS_SUCCESS,
	ADD_LOG_REQUEST,
	ADD_LOG_SUCCESS,
	LOG_TRANSACTION_FAILED
} from '../actions/logs';

/** Calculate and return next ID */
export function calculateNextId (logs) {
	let max = logs.length > 0 ? Math.max.apply(Math, logs.map(x => x.id)) : 0;
	return max + 1;
}

/** Return data from state */
export const fetchState = (state) => state.logs;

/** Retrieve all logs from state */
export function* getLogs () {
	try {
		console.log('get logs saga');
		const logs = select(fetchState);
		console.log(logs)
		yield put({ type: GET_LOGS_SUCCESS, logs });
	} catch (error) {
		yield put({ type: LOG_TRANSACTION_FAILED, error: true, payload: error });
	}
}

/** Add new log in state */
export function* createLog (action) {
	try {
		const logs = yield select(fetchState);
		const id = yield call(calculateNextId, logs);
		const {task, operation} = action.payload;
		console.log(id, task, operation);
		yield put({ type: ADD_LOG_SUCCESS, payload: { log: { id, operation, occurred_in: new Date(), task: `#${task.id} - ${task.item}` }}});
	} catch (error) {
		yield put({ type: LOG_TRANSACTION_FAILED, error: true, payload: error });
	}
}

export default function* rootSaga() {
	yield takeEvery(GET_LOGS_REQUEST, getLogs);
	yield takeEvery(ADD_LOG_REQUEST, createLog);
}
