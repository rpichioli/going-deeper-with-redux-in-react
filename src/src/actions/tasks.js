// Actions ------------------------------------------------------------
export function fetchTasks () { return ({ type: GET_TASKS_REQUEST })}
export function addTask (item) { return ({ type: ADD_TASK_REQUEST, item })}
export function deleteTask (id) { return ({ type: DELETE_TASK_REQUEST, id })}
export function updateTask (task) { return ({ type: UPDATE_TASK_REQUEST, task })}
export function closeAll () { return ({ type: CLOSE_ALL_TASKS_REQUEST })}
export function openAll () { return ({ type: OPEN_ALL_TASKS_REQUEST })}

// Types --------------------------------------------------------------
export const GET_TASKS_REQUEST = 'GET_TASKS_REQUEST';
export const GET_TASKS_SUCCESS = 'GET_TASKS_SUCCESS';
export const ADD_TASK_REQUEST = 'ADD_TASK_REQUEST';
export const ADD_TASK_SUCCESS = 'ADD_TASK_SUCCESS';
export const DELETE_TASK_REQUEST = 'DELETE_TASK_REQUEST';
export const DELETE_TASK_SUCCESS = 'DELETE_TASK_SUCCESS';
export const UPDATE_TASK_REQUEST = 'UPDATE_TASK_REQUEST';
export const UPDATE_TASK_SUCCESS = 'UPDATE_TASK_SUCCESS';
export const CLOSE_ALL_TASKS_REQUEST = 'CLOSE_ALL_TASKS_REQUEST';
export const CLOSE_ALL_TASKS_SUCCESS = 'CLOSE_ALL_TASKS_SUCCESS';
export const OPEN_ALL_TASKS_REQUEST = 'OPEN_ALL_TASKS_REQUEST';
export const OPEN_ALL_TASKS_SUCCESS = 'OPEN_ALL_TASKS_SUCCESS';
export const TASK_TRANSACTION_FAILED = 'TASK_TRANSACTION_FAILED';
