// Actions ------------------------------------------------------------
export function fetchLogs() { return ({ type: GET_LOGS_REQUEST })}

// Types --------------------------------------------------------------
export const ADD_LOG_REQUEST = "ADD_LOG_REQUEST";
export const ADD_LOG_SUCCESS = "ADD_LOG_SUCCESS";
export const GET_LOGS_REQUEST = "GET_LOGS_REQUEST";
export const GET_LOGS_SUCCESS = "GET_LOGS_SUCCESS";
export const LOG_TRANSACTION_FAILED = 'LOG_TRANSACTION_FAILED';
