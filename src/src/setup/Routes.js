import React from 'react';
// Routing
import { Route, Switch } from 'react-router-dom';
// Components
import NotFound from '../components/NotFound';
import Tasklist from '../components/Tasklist';
import Home from '../components/Home';

export default (props) => {
	return (
		<Switch>
			<Route exact path="/" component={Home} />
			<Route exact path="/tasklist" component={Tasklist} />
			<Route component={NotFound} />
		</Switch>
	)
}
