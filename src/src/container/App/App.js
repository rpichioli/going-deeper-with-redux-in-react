import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import clsx from 'clsx';
// Core
import AppBar from '@material-ui/core/AppBar';
import Badge from '@material-ui/core/Badge';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import InputBase from '@material-ui/core/InputBase';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { fade, makeStyles, useTheme, createMuiTheme } from '@material-ui/core/styles';
import { indigo, blue, red } from '@material-ui/core/colors'; // pink, purple, deepPurple, lightBlue, cyan, teal, green, lightGreen, lime, yellow, amber, orange, deepOrange
// Styles
import { ThemeProvider } from '@material-ui/styles';
// Icons
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import HomeIcon from '@material-ui/icons/Home';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import WarningIcon from '@material-ui/icons/Warning';
// Styles
import './App.css';

const drawerWidth = 240;

createMuiTheme({
	palette: {
		type: 'light',
		primary: indigo,
		secondary: blue,
		red: red,
		contrastThreshold: 1,
		tonalOffset: 0.2
	}
});

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
	title: {
    flexGrow: 1,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
	search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
	inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 200,
      },
    },
  },
}));

const App = (props) => {
	const {tasks} = props;
	const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

	const handleDrawerOpen = () => setOpen(true);
  const handleDrawerClose = () => setOpen(false);

  return (
    <ThemeProvider theme={theme}>
			<CssBaseline />
			<div className={classes.root}>
				<AppBar position="fixed" className={clsx(classes.appBar, {
	          [classes.appBarShift]: open,
				})}>
					<Toolbar>
						<IconButton color="inherit" aria-label="open drawer" onClick={handleDrawerOpen} edge="start" className={clsx(classes.menuButton, {
								[classes.hide]: open,
						})} >
							<MenuIcon />
						</IconButton>
						<Typography className={classes.title} variant="h6" noWrap>going-deeper-with-redux-in-redux</Typography>
						<div className={classes.search}>
							<div className={classes.searchIcon}><SearchIcon /></div>
							<InputBase placeholder="Search…" classes={{root: classes.inputRoot, input: classes.inputInput}} inputProps={{ 'aria-label': 'search' }} />
						</div>
					</Toolbar>
				</AppBar>
				<Drawer
					variant="permanent"
					className={clsx(classes.drawer, {
							[classes.drawerOpen]: open,
							[classes.drawerClose]: !open,
					})}
					classes={{
						paper: clsx({
								[classes.drawerOpen]: open,
								[classes.drawerClose]: !open,
						}),
					}}
					open={open}
				>
					<div className={classes.toolbar}>
						<IconButton onClick={handleDrawerClose}>
							{theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
						</IconButton>
					</div>
					<Divider />
					<List>
						<ListItem button key='Homepage' onClick={() => window.location = '/'}>
							<ListItemIcon><HomeIcon /></ListItemIcon>
							<ListItemText primary='Homepage' />
						</ListItem>
						<Divider />
						<ListItem button key='Tasklist' onClick={() => window.location = '/tasklist'}>
							<ListItemIcon>
								{
									tasks.length > 0 ? (
										<Badge badgeContent={tasks.length} color="primary">
											<DoneAllIcon />
										</Badge>
									) : <DoneAllIcon />
								}
							</ListItemIcon>
							<ListItemText primary='Tasklist' />
						</ListItem>
						<Divider />
						<ListItem button key='Invalid' onClick={() => window.location = '/some-invalid-route'}>
							<ListItemIcon><WarningIcon /></ListItemIcon>
							<ListItemText primary='Default not found' />
						</ListItem>
					</List>
				</Drawer>
				<main className={classes.content}>
					<div className={classes.toolbar} />
					{props.children}
				</main>
			</div>
		</ThemeProvider>
  );
}

/** Default properties values */
App.defaultProps = {
	tasks: []
}
/** Properties rules and typing  */
App.propTypes = {
  tasks: PropTypes.array.isRequired,
}

/** Used by react-redux to map state into component properties */
const mapStateToProps = (state, props) => {
	return {
		tasks: state.tasks.filter(x => x.finished === false) || []
	}
}

export default connect(mapStateToProps, {})(App);
