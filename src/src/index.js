import React from 'react';
import ReactDOM from 'react-dom';
// Internal - Service workers
import * as serviceWorker from './serviceWorker';
// Routing
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
// Redux
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
// import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from './reducers/rootReducer';
import { loadState, saveState } from './utils/localStorage';
// Utils
import throttle from 'lodash/throttle'; // Invokes once until wait time expires
// Container
import App from './container/App/App'; // First level component
// Routes
import Routes from './setup/Routes'; // All routes exported from stateless component
// Sagas middleware
import tasksSaga from './sagas/tasks';
import logsSaga from './sagas/logs';

// Browser history
const history = createBrowserHistory();
// Saga middleware
const sagaMiddleware = createSagaMiddleware();
// Get state present in localStorage to set into store
const persistedState = loadState();
// Redux store -> Combined reducers and middleware for real-time monitoring
const store = createStore(
	rootReducer,
	persistedState,
	composeWithDevTools(applyMiddleware(sagaMiddleware))
);

/** Intercepts each state update and persist it in localStorage */
store.subscribe(throttle(() => {
	// ----- We have 2 options here -----
	// - 1. Update ALL data present in state
	saveState(store.getState());
	// - 2. Update ONLY some state collections
	// saveState({ the-collection: store.getState().the-collection });
}), 2000);

sagaMiddleware.run(tasksSaga)
sagaMiddleware.run(logsSaga)

ReactDOM.render(
	<Provider store={store}>
		<Router history={history}>
			<App><Routes /></App>
		</Router>
	</Provider>,
document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
