import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {makeStyles, withStyles, Paper, FormControl, InputLabel, Input, FormHelperText, Typography, Container, Button, DialogTitle, DialogContent, DialogContentText, DialogActions, Dialog, Table, TableBody, TableCell, TableHead, TableRow, Checkbox, IconButton} from '@material-ui/core';
import {SpeedDial, SpeedDialIcon, SpeedDialAction} from '@material-ui/lab';
import Add from '@material-ui/icons/Add';
import DoneAll from '@material-ui/icons/DoneAll';
import ClearAll from '@material-ui/icons/ClearAll';
import PrintIcon from '@material-ui/icons/Print';
import ShareIcon from '@material-ui/icons/Share';
import RemoveCircle from '@material-ui/icons/RemoveCircle';
import {addTask, deleteTask, updateTask, closeAll, openAll} from '../actions/tasks';
import {fetchLogs} from '../actions/logs';

/** Material theme stuff */
const useStyles = makeStyles(theme => ({
	root: { padding: theme.spacing(3, 2) },
	speedDial: { position: 'absolute', bottom: theme.spacing(2), right: theme.spacing(3) },
	paper: { marginTop: theme.spacing(3), width: '100%', overflowX: 'auto', marginBottom: theme.spacing(2) },
}));

const CustomCheckbox = withStyles({
  root: {
    color: "#FFF",
    '&$checked': {
      color: "#DDD",
    },
  },
  checked: {},
})(props => <Checkbox color="default" {...props} />);

const DataTableCell = withStyles(theme => ({
  head: {
		backgroundColor: "#5c6bc0",
		color: theme.palette.common.white,
	},
}))(TableCell);

const LogTableCell = withStyles(theme => ({
  head: {
		backgroundColor: "#5c6bc0",
		color: theme.palette.common.white,
	}
}))(TableCell);

const StripedTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: "#eceff1",
    },
  },
}))(TableRow);

/** Tasklist component - Stateless + Hooks */
const Tasklist = (props) => {
	const classes = useStyles(); // Getting our Material custom styles overriding the theme
	const {logs, tasks, finishedTasks} = props; // Getting mapped props mapped from Redux state

	// Hooks
	const [task, setTask] = useState("");
	const [errors, setErrors] = useState({});
	const [selectedTask, setSelectedTask] = useState(null);
	const [showSpeedDial, setShowSpeedDial] = useState(false);
	const [showDeletionConfirmationDialog, setShowDeletionConfirmationDialog] = useState(false);
	const [showUpdateConfirmationDialog, setShowUpdateConfirmationDialog] = useState(false);
	const [showNewTaskDialog, setShowNewTaskDialog] = useState(false);
	useEffect(() => setTask(""), [showNewTaskDialog]); // Reset task when new task dialog is being called
	const [log, setLog] = useState([]);
	useEffect(() => {
		props.fetchLogs()
	}, []); // Call fetch logs action to get most recent store data

	// Static data structures
	const speedDialActions = [
		{ icon: <Add />, name: 'New' },
		{ icon: <DoneAll />, name: 'Close all' },
		{ icon: <ClearAll />, name: 'Open all' },
		{ icon: <PrintIcon />, name: 'Print' },
		{ icon: <ShareIcon />, name: 'Share' },
	];

	// === DIALOGS MANAGEMENT ====================================================

	/** Select task to use in confirm selection */
	const confirmTaskDeletion = (task) => {
		setSelectedTask(task);
		setShowDeletionConfirmationDialog(true);
	}

	/** Select task to use in confirm selection */
	const confirmTaskUpdate = (task) => {
		setSelectedTask(task);
		setShowUpdateConfirmationDialog(true);
	}

	// === ACTIONS TRIGGERED BY CONFIRMATION======================================

	/** Proceed with Redux dispatch */
	const taskDeletionConfirmed = () => {
		setShowDeletionConfirmationDialog(false);
		props.deleteTask(selectedTask.id)
		setSelectedTask(null);
	}

	/** Proceed with Redux dispatch */
	const taskUpdateConfirmed = () => {
		setShowUpdateConfirmationDialog(false);
		props.updateTask(selectedTask)
		setSelectedTask(null);
	}

	// === EVENTS ================================================================

	const onSelectAllClick = (e) => {
		// alert(e.target.checked);
		if (e.target.checked)
			props.closeAll();
		else
			props.openAll();
	}

	// === FORM MANAGEMENT =======================================================

	/** New task form validation */
	const validateForm = () => {
		let errors = {};
		if (!task) errors.task = 'Provide the task information to proceed';
		return { errors, isValid: Object.keys(errors).length === 0 };
	}

	/** Validate */
	const submitTask = () => {
		const {errors, isValid} = validateForm();
		if (isValid) {
			setShowNewTaskDialog(false);
			setErrors({});
			props.addTask(task);
		} else {
			setErrors(errors);
		}
	}

  return (
		<Typography paragraph>
			<Typography color="text.primary" variant="h4">Tasklist</Typography>
			<Typography color="text.secondary" variant="h6">Stateful application using the "powers" of Redux</Typography>
			<br />

			{/* Main content */}
			<Container maxWidth="md">
				<Paper className={classes.paper}>
					<Table size="small">
						<TableHead>
							<TableRow>
								<DataTableCell width="5%" padding="checkbox" align="center">
									<Checkbox
										color="black"
										inputProps={{ 'aria-label': 'select all tasks' }}
										onChange={onSelectAllClick}
										checked={finishedTasks.length === tasks.length}
										indeterminate={finishedTasks.length > 0 && finishedTasks.length !== tasks.length}
									/>
								</DataTableCell>
								<DataTableCell width="5%" align="center">#</DataTableCell>
								<DataTableCell>Task</DataTableCell>
								<DataTableCell width="10%" align="center">Actions</DataTableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{
								tasks.map((row, index) => {
	          		const labelId = `enhanced-table-checkbox-${index}`;
									return (
										<StripedTableRow role="checkbox" aria-checked={row.finished} tabIndex={-1} key={row.id} selected={row.finished} hover>
											<TableCell padding="checkbox" align="center">
												<Checkbox
													color="black"
													onChange={() => confirmTaskUpdate(row)}
													checked={row.finished}
													inputProps={{ 'aria-labelledby': labelId }}
												 />
											</TableCell>
											<TableCell align="center">{row.id}</TableCell>
											<TableCell>{row.item}</TableCell>
											<TableCell align="center">
												<IconButton onClick={() => confirmTaskDeletion(row)} aria-label="delete" size="small">
													<RemoveCircle fontSize="small" />
												</IconButton>
											</TableCell>
										</StripedTableRow>
									);
								})
							}
						</TableBody>
					</Table>
				</Paper>

				{logs.length > 0 &&
				<Paper className={classes.paper}>
					<Table size="small">
	          <TableHead>
	            <TableRow>
	              <LogTableCell align="center" width="5%">#</LogTableCell>
	              <LogTableCell>Task involved</LogTableCell>
								<LogTableCell align="left" width="15%">Action</LogTableCell>
								<LogTableCell align="center" width="20%">Occurred in</LogTableCell>
	            </TableRow>
	          </TableHead>
	          <TableBody>
	            {logs.map(log => (
	              <StripedTableRow key={log.id}>
	                <TableCell align="center" component="th" scope="row">{log.id}</TableCell>
	                <TableCell>{log.task}</TableCell>
									<TableCell align="left">{log.operation}</TableCell>
									<TableCell align="center">{new Date(log.occurred_in).toLocaleString('pt-br')}</TableCell>
	              </StripedTableRow>
	            ))}
	          </TableBody>
	        </Table>
				</Paper>}
			</Container>

			{/* Application page toolbar */}
			<SpeedDial
				ariaLabel="SpeedDial toolbar for tasklist options"
				className={classes.speedDial}
				direction="left"
				icon={<SpeedDialIcon />}
				onBlur={() => setShowSpeedDial(false)}
				onClick={() => setShowSpeedDial(true)}
				onClose={() => setShowSpeedDial(false)}
				onFocus={() => setShowSpeedDial(true)}
				onMouseEnter={() => setShowSpeedDial(true)}
				onMouseLeave={() => setShowSpeedDial(false)}
				open={showSpeedDial}
			>
				{speedDialActions.map(action => (
					<SpeedDialAction
						key={action.name}
						icon={action.icon}
						tooltipTitle={action.name}
						// tooltipOpen
						onClick={() => {
							setShowSpeedDial(!showSpeedDial);
							switch(action.name) {
								case 'New': setShowNewTaskDialog(true); break;
								default: // Nothing
							}
						}
						}
					/>
				))}
			</SpeedDial>

			{/* New task dialog */}
			<Dialog open={showNewTaskDialog} aria-labelledby="new-task-form" maxWidth="xs" disableBackdropClick disableEscapeKeyDown>
				<DialogTitle id="new-task-form">Add new task</DialogTitle>
				<DialogContent>
					<DialogContentText>
						Add new task easily just providing it's description and clicking "Add" button.
          </DialogContentText>
					<FormControl fullWidth error={errors.task} required>
						<InputLabel htmlFor="task">What is the task?</InputLabel>
						<Input id="task" name="task" value={task} onChange={e => setTask(e.target.value)} placeholder="e.g. Read React Hooks documentation" aria-describedby="task-text" />
						<FormHelperText id="task-text">{errors.task}</FormHelperText>
					</FormControl>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setShowNewTaskDialog(false)} color="secondary">Cancel</Button>
          <Button onClick={() => submitTask()} color="primary">Add</Button>
        </DialogActions>
			</Dialog>

			{/* Update confirmation dialog */}
			<Dialog open={showUpdateConfirmationDialog} aria-labelledby="confirmation-dialog-title" maxWidth="xs" disableBackdropClick disableEscapeKeyDown>
				<DialogTitle id="confirmation-dialog-title">Confirmation is required</DialogTitle>
				<DialogContent dividers>
					<p>Proceeding will change task "<u>{selectedTask && selectedTask.item}</u>" status permanently.</p>
				</DialogContent>
				<DialogActions>
					<Button onClick={() => {
						setSelectedTask(null);
						setShowUpdateConfirmationDialog(false);
					}} color="secondary">Cancel and close</Button>
					<Button onClick={() => taskUpdateConfirmed()} color="primary">Ok, I confirm</Button>
				</DialogActions>
			</Dialog>

			{/* Delete confirmation dialog */}
			<Dialog open={showDeletionConfirmationDialog} aria-labelledby="confirmation-dialog-title" maxWidth="xs" disableBackdropClick disableEscapeKeyDown>
				<DialogTitle id="confirmation-dialog-title">Confirmation is required</DialogTitle>
				<DialogContent dividers>
					<p>Proceeding will remove the task "<u>{selectedTask && selectedTask.item}</u>" permanently.</p>
				</DialogContent>
				<DialogActions>
					<Button onClick={() => {
						setSelectedTask(null);
						setShowDeletionConfirmationDialog(false);
					}} color="secondary">Cancel and close</Button>
					<Button onClick={() => taskDeletionConfirmed()} color="primary">Ok, I confirm</Button>
				</DialogActions>
			</Dialog>
		</Typography>
  );
}

/** Default properties values */
Tasklist.defaultProps = {
	tasks: []
}
/** Properties rules and typing  */
Tasklist.propTypes = {
  tasks: PropTypes.array.isRequired,
}

/** Used by react-redux to map state into component properties */
const mapStateToProps = (state, props) => {
	return {
		tasks: state.tasks || [],
		finishedTasks: state.tasks.filter(x => x.finished === true) || [],
		logs: state.logs || []
	}
}

/** Used by react-redux to map all actions to be dispatched into component properties */
const mapDispatchToProps = (dispatch, ownProps) => ({
	addTask: item => dispatch(addTask(item)),
	updateTask: task => dispatch(updateTask(task)),
	deleteTask: id => dispatch(deleteTask(id)),
	closeAll: () => dispatch(closeAll()),
	openAll: () => dispatch(openAll()),
	fetchLogs: () => dispatch(fetchLogs())
})

export default connect(mapStateToProps, mapDispatchToProps)(Tasklist);
