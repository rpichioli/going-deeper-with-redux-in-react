import React from 'react';
import {Typography, Container} from '@material-ui/core';
import red from '@material-ui/core/colors/red';

class NotFound extends React.Component {
  render() {
		const primary = red[800];
    return (
      <React.Fragment>
				<Typography color="error" variant="h4">Ops, page not found</Typography>
				<Typography color="error" paragraph>Try to use valid the valid routes in <u>menu, at left</u>.</Typography>
			</React.Fragment>
    );
  }
}

export default NotFound;
