import React from 'react';
import {makeStyles, Typography, Container, Paper} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
	root: { padding: theme.spacing(3, 2) },
	paper: { marginTop: theme.spacing(3), width: '100%', overflowX: 'auto', marginBottom: theme.spacing(2), padding: theme.spacing(3) },
	list: { paddingLeft: theme.spacing(2) }
}));

const Home = () => {
	const classes = useStyles(); // Getting our Material custom styles overriding the theme
  return (
		<React.Fragment>
			<Typography color="text.primary" variant="h4">The project</Typography>
			<Typography color="text.secondary" variant="h6">About and the motivation to create it</Typography>
			<Paper className={classes.paper}>
				<Typography color="text.primary" variant="h6">What it is about?</Typography>
				<p>The project consists mainly in explore Redux going deeper within React applications.</p>
				<Typography color="text.primary" variant="h6">What you will find?</Typography>
				<p>Here you will see different techniques, redux-saga middleware, best practices with react-redux, usage tips, samples and some magics too!</p>
				<Typography color="text.primary" variant="h6">Some points of interest</Typography>
				<ul className={classes.list}>
					<li>Map both state and dispatches to props via store connection (react-redux).</li>
					<li>Apply redux-saga as Redux middleware controlling 2 states from store</li>
					<li>Using rootSaga to trigger business rules and state interactions based in dispatched actions, dispatching others, identifying failure or success.</li>
					<li>Synchronize Redux store with localStorage, like an offline (browser) state database.</li>
				</ul>
			</Paper>
		</React.Fragment>
  );
}

export default Home;
