import {
	GET_LOGS_SUCCESS,
	ADD_LOG_SUCCESS,
	LOG_TRANSACTION_FAILED
} from '../actions/logs';

export default (state = [], action = {}) => {
	switch (action.type) {
		case GET_LOGS_SUCCESS:
			return action.payload.logs;
		case ADD_LOG_SUCCESS:
			return [...state, action.payload.log];
		case LOG_TRANSACTION_FAILED:
			return state;
		default:
			return state;
	}
}
