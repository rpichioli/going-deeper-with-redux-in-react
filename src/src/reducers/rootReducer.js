import { combineReducers } from 'redux';
import tasks from './tasks';
import logs from './logs';

export default combineReducers({ tasks, logs });
