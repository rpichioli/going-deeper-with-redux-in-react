import {
	GET_TASKS_SUCCESS,
	ADD_TASK_SUCCESS,
	DELETE_TASK_SUCCESS,
	UPDATE_TASK_SUCCESS,
	CLOSE_ALL_TASKS_SUCCESS,
	OPEN_ALL_TASKS_SUCCESS,
	TASK_TRANSACTION_FAILED
} from '../actions/tasks';

const initialState = [
	{id: 1, item: 'Listen to Iron Maiden at least 30 minutes', finished: true},
	{id: 2, item: 'Take a look at Palmeiras\'s news', finished: false},
	{id: 3, item: 'Apply redux-saga in this app', finished: false},
	{id: 4, item: 'Play some CS:GO', finished: true},
];

/** Tasks reducer */
export default (state = initialState, action = {}) => {
	switch (action.type) {
		case GET_TASKS_SUCCESS:
			return action.payload.tasks;
		case ADD_TASK_SUCCESS:
			return [...state, action.payload.task];
		case DELETE_TASK_SUCCESS:
			return state.filter(x => x.id !== action.payload.task.id);
		case UPDATE_TASK_SUCCESS:
			const newCollection = [...state];
			const index = newCollection.findIndex(x => x.id === action.payload.task.id);
			if (index >= 0) newCollection[index].finished = action.payload.task.finished;
			return newCollection;
		case CLOSE_ALL_TASKS_SUCCESS:
			return state.map(item => {
				item.finished = true;
				return item;
			});
		case OPEN_ALL_TASKS_SUCCESS:
			return state.map(item => {
				item.finished = false;
				return item;
			});
		case TASK_TRANSACTION_FAILED:
			return state;
		default:
			return state;
	}
}
