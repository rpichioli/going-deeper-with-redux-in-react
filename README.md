# going-deeper-with-redux-in-react

Going deeper in Redux within React with tricks like: redux-saga middlewares triggering sagas from others, react hooks and lifecycle inside stateless component, handle store properly, sync store with localStorage in every action dispatched, and more.

## React Hooks, Stateless component and react-redux
We are going to use hooks to apply state and lifecyle in each one inside stateless (function) React components.

With hooks we can avoid wrapper hell with High Order Components (HOC), avoid the mass and duplicity of code in each class component, avoid duplicity of code in lifecycle methods, classes structure needs, and more..

We are going to work with stateless component like a stateful one, with whole logic built exclusively for our need with no "trash" or unnecessary code / methods / whathever.

## Sagas middleware
With sagas we can treat actions, dispatches and build complex Redux processing flow / logic in a dedicated layer.

You'll see the different types we can have for an action caring about it's state like request, success or failure (I'm using a generic failure action).

We will work with an task saga caring about the records with some request actions triggered by our component by user interaction, and we will have to a log saga which will be called by task saga based in it's flow and just a fetch action to work with UI.

## Relevant topics
-   Map both state and dispatches to props via store connection (react-redux).
-   Apply redux-saga as Redux middleware controlling 2 states from store
-   Using rootSaga to trigger business rules and state interactions based in dispatched actions, dispatching others, identifying failure or success.
-   Synchronize Redux store with localStorage, like an offline (browser) state database.

## Requirements
Be sure you have **node** and **npm** installed, you can download easily in the [official website](https://nodejs.org/en/download/).

After the installation you can open the terminal and test both node and npm typing:
```
node -v
```

This way you'll get node's respective version if the installation was fully OK.
```
npm -v
```

This way you test the npm, the same way you test node.

If you have both messages you are ready to go! :)

The **npm** is used to install and manage project dependencies and run it by command line, **becomes with node instalation**.


## How to use
With everything configured, open your terminal and go to the `src` folder under the root of the repository you cloned.

To install all the dependencies you must run:
```
npm i
```

Wait a little bit until the installation is completed.
This process may take a while.

With installation completed, you just need to start the application typing:
```
npm start
```

To stop the running application press `ctrl + c`.
The console will question about stopping the application, just type `S` and `enter`.

That's all!


## Developed by
Rodrigo Quiñones Pichioli - since July 4, 2019
